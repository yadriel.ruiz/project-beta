# CarCar

Team:

* D'Angelo DeNiro - Service
* Yadriel Ruiz - Sales

## Design

![Alt text](pfp.png)

## Service microservice

Explain your models and integration with the inventory
microservice, here.

Models:

-Technician contains a Service Technicians first name, last name and employee ID. These technicians are able to be assigned to Service Appointments.
    Properties: ID, first_name, last_name, employee_id

-AutomobileVO Utlizes poller.py allowing the integration of data between the Inventory models, specifically Automobiles to other microservices. The AutomobileVO object stores the VIN and Sold (whether or not the vehicle was purchased at this dealership) properties.
    Properties: VIN, sold

-Appointment contains a date and time, reason, status (created, cancelled, finished), car's VIN number, customer and technician object. The technician property is a Foreign Key of the Technician object and is set using a created technicians ID. The appointment integrates with the Inventory, and Sales microservices, checking the VIN against cars that were sold. If the specifiied VIN of the car matches one of a car that was sold at this dealership, "VIP" is noted on the Appointment list to designate appointments with a preferential status.
    Properties: ID, date_time, reason, status, VIN, customer, technician

## Sales microservice

This microservice handles sales-related functionalities. Below are the models used and their purposes:

Salesperson:

Attributes: First name, last name, employee ID.
Purpose: Enables assignment of salespersons to sales transactions.

Customer:

Attributes: First name, last name, address, phone number.
Purpose: Allows recording of customer information associated with sales transactions.

AutomobileVO:

Utilizes poller.py for integration with the inventory microservice.
Attributes: VIN (Vehicle Identification Number), sold status.
Purpose: Retrieves specific information about automobiles from the inventory microservice, facilitating their association with sales transactions.

Sale:

Attributes: Salesperson(ForeignKey), Customer(ForeignKey), AutomobileVO(ForeignKey), Price.
Purpose: Records details of sales transactions, including salesperson, customer, automobile information and price.


## Instructions for running this application

- In command prompt cd into the empty directory in which you would like the project to be stored.
- To clone via HTTPS, run: git clone https://gitlab.com/denireaux/project-beta.git .
- Ensure that Docker desktop is installed, Docker download: https://www.docker.com/products/docker-desktop/
- Build and start database, and Docker containers:
    - docker volume create beta-data
    - docker-compose build
    - docker-compose up
- Ensure all containers are built and running in Docker Desktop or via command line.
- In your browser, navigate to http://localhost:3000


## Where each microservice is running

- React: http://localhost:3000
- Service: http://localhost:8080
- Sales: http://localhost:8090
- Inventory: http://localhost:8100


## Bounded Contexts (BC) and associated Value Objects (VO)

Bounded Contexts and associated Value Objects:
    - Service (BC)
        - Appointment
            - ID (VO)
            - date_time (VO)
            - reason (VO)
            - status (VO)
            - VIN (VO)
            - customer (VO)
            - technician
        - AutomobileVO
            - ID (VO)
            - VIN (VO)
            - sold (VO)
        - Technician 
            - ID (VO)
            - first_name (VO)
            - last_name (VO)
            - employee_id (VO)
    - Inventory (BC)
        - Automobile
            - ID (VO)
            - color (VO)
            - year (VO)
            - VIN (VO)
            - sold (VO)
            - model
        - Vehicle Model
            - ID (VO)
            - name (VO)
            - picture_url (VO)
            - manufacturer
        - Manufacturer
            - ID (VO)
            - name (VO)
    - Sales (BC)
        - Sale
            - ID (VO)
            - salesperson
            - customer
            - automobile
            - price (VO)
        - Customer
            - ID (VO)
            - first_name (VO)
            - last_name (VO)
            - address (VO)
            - phone_number (VO)
        - Salesperson
            - ID (VO)
            - first_name (VO)
            - last_name (VO)
            - employee_id
        - AutomobileVO
            - ID (VO)
            - VIN (VO)
            - sold (VO)


Manufacturers:

    List manufacturers	
        GET	
        http://localhost:8100/api/manufacturers/

    Create a manufacturer	
        POST	
        http://localhost:8100/api/manufacturers/

    Get a specific manufacturer	
        GET	
        http://localhost:8100/api/manufacturers/:id/

    Update a specific manufacturer	
        PUT	
        http://localhost:8100/api/manufacturers/:id/

    Delete a specific manufacturer	
        DELETE	
        http://localhost:8100/api/manufacturers/:id/


    Creating a manufacturer:
        {
            "name": "Chrysler"
        }

    Manufacturer object return:
        {
            "href": "/api/manufacturers/1/",
            "id": 1,
            "name": "Chrysler"
        }


Vehicle Models:

    List vehicle models	
        GET	
        http://localhost:8100/api/models/

    Create a vehicle model	
        POST	
        http://localhost:8100/api/models/

    Get a specific vehicle model	
        GET	
        http://localhost:8100/api/models/:id/

    Update a specific vehicle model	
        PUT	
        http://localhost:8100/api/models/:id/

    Delete a specific vehicle model	
        DELETE	
        http://localhost:8100/api/models/:id/


        Creating a vehicle model:
            {
                "name": "Sebring",
                "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
                "manufacturer_id": 1
            }

        Vehicle Model object return:
            {
                "href": "/api/models/1/",
                "id": 1,
                "name": "Sebring",
                "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
                "manufacturer": {
                    "href": "/api/manufacturers/1/",
                    "id": 1,
                    "name": "Daimler-Chrysler"
                }
            }


Automobiles:

    List automobiles	
        GET	
        http://localhost:8100/api/automobiles/

    Create an automobile	
        POST	
        http://localhost:8100/api/automobiles/

    Get a specific automobile	
        GET	
        http://localhost:8100/api/automobiles/:vin/

    Update a specific automobile	
        PUT	
        http://localhost:8100/api/automobiles/:vin/

    Delete a specific automobile	
        DELETE	
        http://localhost:8100/api/automobiles/:vin/


        Creating an automobile:
            {
                "color": "red",
                "year": 2012,
                "vin": "1C3CC5FB2AN120174",
                "model_id": 1
            }

        Automobile object return:
            {
                "href": "/api/automobiles/1C3CC5FB2AN120174/",
                "id": 1,
                "color": "yellow",
                "year": 2013,
                "vin": "1C3CC5FB2AN120174",
                "model": {
                    "href": "/api/models/1/",
                    "id": 1,
                    "name": "Sebring",
                    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
                    "manufacturer": {
                    "href": "/api/manufacturers/1/",
                    "id": 1,
                    "name": "Daimler-Chrysler"
                    }
                },
                "sold": false
            }




Technicians:

    List technicians	
        GET	
        http://localhost:8080/api/technicians/
    
    Create a technician	
        POST	
        http://localhost:8080/api/technicians/
    
    Delete a specific technician	
        DELETE	
        http://localhost:8080/api/technicians/:id/


        Creating a technician:
            {
                "first_name": "Yadriel",
                "last_name": "Ruiz",
                "employee_id": "yaya"
            }

        Technician object return:
            {
                "first_name": "Yadriel",
                "last_name": "Ruiz",
                "employee_id": "yaya"
            }


Appointments:

    List appointments	
        GET	
        http://localhost:8080/api/appointments/
    
    Create an appointment	
        POST	
        http://localhost:8080/api/appointments/
    
    Delete an appointment	
        DELETE	
        http://localhost:8080/api/appointments/:id/


        Creating an appointment:
            {
                "date_time": "2024-03-21T14:10",
                "reason": "Car alarm won't stop going off.",
                "status": "created",
                "vin": "5C3CC5FB2AN120175",
                "customer": "Susan Sujin Bai",
                "technician": "1"
            }

        Appointment object return:
            {
                "date_time": "2024-03-19T21:29:40+00:00",
                "reason": "Car alarm won't stop going off.",
                "status": "created",
                "vin": "2C3CC5FB2AN120175",
                "customer": "Susan Sujin Bai",
                "technician": 1,
                "id": 1
            }


Appointment Status:

    Set appointment status to "canceled"	
        PUT	
        http://localhost:8080/api/appointments/:id/cancel/

    Set appointment status to "finished"	
        PUT	
        http://localhost:8080/api/appointments/:id/finish/


Salespeople:

    List salespeople	
        GET	
        http://localhost:8090/api/salespeople/

    Create a salesperson	
        POST	
        http://localhost:8090/api/salespeople/

    Delete a specific salesperson	
        DELETE	
        http://localhost:8090/api/salespeople/:id/


        Creating a salesperson:
            {
                "first_name": "Michael",
                "last_name": "Scott",
                "employee_id": "117"
            }

        Salesperson object return:
            {
                "id": 1,
                "first_name": "Michael",
                "last_name": "Scott",
                "employee_id": "117"
            }


Customers:

    List customers	
        GET	
        http://localhost:8090/api/customers/

    Create a customer	
        POST	
        http://localhost:8090/api/customers/

    Delete a specific customer	
        DELETE	
        http://localhost:8090/api/customers/:id/


    Creating a customer:
        {
            "first_name": "Yadriel",
            "last_name": "Ruiz",
            "address": "Houston, TX",
            "phone_number": "16546541654"
        }

    Customer object return:
        {
            "id": 1,
            "first_name": "Yadriel",
            "last_name": "Ruiz",
            "address": "Houston, TX",
            "phone_number": "16546541654"
        }


Sales:

    List sales	
        GET	
        http://localhost:8090/api/sales/

    Create a sale	
        POST	
        http://localhost:8090/api/sales/

    Delete a sale	
        DELETE	
        http://localhost:8090/api/sales/:id

    
    Creating a sale:
        {
            "automobile": "1C3CC5FB2AN120174",
            "customer": 1,
            "price": 8999.99,
            "salesperson": 1
        }

    Sale object return
        {
            "id": 1,
            "automobile": "1C3CC5FB2AN120174",
            "salesperson": "Michael Scott",
            "customer": "Yadriel Ruiz",
            "price": 8999.19,
            "employee_id": "117"
        }