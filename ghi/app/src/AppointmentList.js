import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";

function AppointmentList() {
    const fetchVIPStatus = async (vin) => {
        const automobilesURL = `http://localhost:8100/api/automobiles/${vin}/`;
        let response;

        try {
            response = await fetch(automobilesURL).catch((error) =>
                console.error("Caught error in catch.")
            );
        } catch {
            console.error("Caught error in catch.");
        }
        if (response.ok) {
            const data = await response.json();
            return data.sold;
        }
    };

    const fetchAppointments = async () => {
        const appointmentsURL = "http://localhost:8080/api/appointments/";
        const response = await fetch(appointmentsURL);
        if (response.ok) {
            const data = await response.json();
            return data.appointments;
        } else {
            console.error("Failed to fetch.");
        }
    };

    const formatDateTime = (dateTimeString) => {
        const dateTime = new Date(dateTimeString);
        return dateTime.toLocaleString();
    };

    const handleCancelClicked = async (id) => {
        const url = `http://localhost:8080/api/appointments/${id}/cancel/`;
        const fetchConfig = {
            method: "PUT",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ status: "cancelled" }),
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            window.alert("Appointment successfully cancelled.");
            window.location.reload();
        }
    };

    const handleFinishClicked = async (id) => {
        const url = `http://localhost:8080/api/appointments/${id}/finish/`;

        const fetchConfig = {
            method: "PUT",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ status: "finished" }),
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            window.alert("Appointment successfully finished.");
            window.location.reload();
        }
    };

    const [appointments, setAppointments] = useState([]);

    useEffect(() => {
        const appointments = async () => {
            const createdAppointments = await fetchAppointments();
            const filteredAppointments = createdAppointments.filter(
                (appointment) => appointment.status === "created"
            );
            const updatedAppointments = [];

            for (const appointment of filteredAppointments) {
                const isVIP = await fetchVIPStatus(appointment.vin);
                const updatedAppointment = { ...appointment, isVIP };
                updatedAppointments.push(updatedAppointment);
            }
            setAppointments(updatedAppointments);
        };
        appointments();
    }, []);

    return (
        <>
            {appointments.length ? (
                <div>
                    <br />
                    <p className="h1">Service Appointments</p>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>VIP Status</th>
                                <th>Customer</th>
                                <th>VIN</th>
                                <th>Date and Time</th>
                                <th>Technician</th>
                                <th>Reason</th>
                            </tr>
                        </thead>
                        <tbody>
                            {appointments.map((appointment) => (
                                <tr key={appointment.id}>
                                    <td>{appointment.isVIP ? "VIP" : ""}</td>
                                    <td>{appointment.customer}</td>
                                    <td>{appointment.vin}</td>
                                    <td>
                                        {formatDateTime(appointment.date_time)}
                                    </td>
                                    <td>{appointment.technician}</td>
                                    <td>{appointment.reason}</td>
                                    <td>
                                        <button
                                            onClick={() =>
                                                handleCancelClicked(
                                                    appointment.id
                                                )
                                            }
                                            id="cancel-button"
                                            type="button"
                                            className="btn btn-danger"
                                        >
                                            Cancel
                                        </button>
                                    </td>
                                    <td>
                                        <button
                                            onClick={() =>
                                                handleFinishClicked(
                                                    appointment.id
                                                )
                                            }
                                            id="finish-button"
                                            type="button"
                                            className="btn btn-success"
                                        >
                                            Finish
                                        </button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            ) : (
                <>
                    <h1>No Appointments</h1>
                    <p>There are no appointments in the database</p>
                    <NavLink
                        to="/appointments/create"
                        className="btn btn-success"
                    >
                        Add Appointment
                    </NavLink>
                </>
            )}
        </>
    );
}

export default AppointmentList;
