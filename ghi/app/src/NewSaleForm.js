import React, { useState, useEffect } from "react";

function NewSaleForm() {
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);

    const [salesperson, setSalesperson] = useState("");
    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    };

    const [customer, setCustomer] = useState("");
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    };

    const [automobile, setAutomobile] = useState("");
    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    };

    const [price, setPrice] = useState("");
    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    };

    const fetchSalespeople = async () => {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salesperson);
        }
    };

    const fetchCustomers = async () => {
        const url = "http://localhost:8090/api/customers/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    };

    const fetchAutomobiles = async () => {
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            const availableAutomobiles = data.autos.filter(
                (auto) => auto.sold === false
            );
            setAutomobiles(availableAutomobiles);
        }
    };

    useEffect(() => {
        fetchSalespeople();
        fetchCustomers();
        fetchAutomobiles();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.salesperson = salesperson;
        data.customer = customer;
        data.automobile = automobile;
        data.price = price;

        const url = "http://localhost:8090/api/sales/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newSale = await response.json();
            setSalesperson("");
            setCustomer("");
            setAutomobile("");
            setPrice("");
        }

        const autoUrl = `http://localhost:8100/api/automobiles/${automobile}/`;
        const autoFetchConfig = {
            method: "PUT",
            body: JSON.stringify({ sold: true }),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const autoResponse = await fetch(autoUrl, autoFetchConfig);
        if (autoResponse.ok) {
            const autoData = await autoResponse.json();
        }

        const formTag = document.getElementById("create-customer-form");
        formTag.reset();
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a Sale</h1>
                    <form onSubmit={handleSubmit} id="create-customer-form">
                        <div className="dropdown mb-3">
                            <select
                                value={salesperson}
                                onChange={handleSalespersonChange}
                                className="form-select form-select-lg mb-3"
                                aria-label="Large select example"
                            >
                                <option value="">
                                    Select Sales Representative
                                </option>
                                {salespeople.map((salesperson) => {
                                    return (
                                        <option
                                            key={salesperson.id}
                                            value={salesperson.id}
                                        >
                                            {" "}
                                            {salesperson.first_name}{" "}
                                            {salesperson.last_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="dropdown mb-3">
                            <select
                                value={customer}
                                onChange={handleCustomerChange}
                                className="form-select form-select-lg mb-3"
                                aria-label="Large select example"
                            >
                                <option value="">Select Customer</option>
                                {customers.map((customer) => {
                                    return (
                                        <option
                                            key={customer.id}
                                            value={customer.id}
                                        >
                                            {" "}
                                            {customer.first_name}{" "}
                                            {customer.last_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="dropdown mb-3">
                            <select
                                value={automobile}
                                onChange={handleAutomobileChange}
                                className="form-select form-select-lg mb-3"
                                aria-label="Large select example"
                            >
                                <option value="">Select Vehicle</option>
                                {automobiles.map((auto) => {
                                    return (
                                        <option key={auto.vin} value={auto.vin}>
                                            {" "}
                                            {auto.year} {auto.model["name"]}{" "}
                                            {auto.model["manufacturer"]["name"]}{" "}
                                            - {auto.vin}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                value={price}
                                onChange={handlePriceChange}
                                type="text"
                                className="form-control"
                                id="floatingInput"
                            />
                            <label htmlFor="floatingInput">Price</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default NewSaleForm;
