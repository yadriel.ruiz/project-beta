import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";

function AutomobilesList() {
    const [automobiles, setAutomobiles] = useState([]);

    const fetchAutomobiles = async () => {
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    };

    useEffect(() => {
        fetchAutomobiles();
    }, []);

    return (
        <>
            {automobiles.length ? (
                <div>
                    <br></br>
                    <p className="h1">Automobiles List</p>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>VIN</th>
                                <th>Color</th>
                                <th>Year</th>
                                <th>Model</th>
                                <th>Manufacturer</th>
                                <th>Sold?</th>
                            </tr>
                        </thead>
                        <tbody>
                            {automobiles.map((auto) => {
                                return (
                                    <tr key={auto.id}>
                                        <td>{auto.vin}</td>
                                        <td>{auto.color}</td>
                                        <td>{auto.year}</td>
                                        <td>{auto.model.name}</td>
                                        <td>{auto.model.manufacturer.name}</td>
                                        <td>{auto.sold ? "Yes" : "No"}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            ) : (
                <>
                    <h1>No Automobiles</h1>
                    <p>There are no automobiles in the database</p>
                    <NavLink
                        to="/automobiles/create"
                        className="btn btn-success"
                    >
                        Add an Automobile
                    </NavLink>
                </>
            )}
        </>
    );
}

export default AutomobilesList;
