import React, { useState, useEffect } from "react";

function CreateAutomobile() {
    const [models, setModels] = useState([]);
    const [color, setColor] = useState("");
    const [year, setYear] = useState("");
    const [vin, setVin] = useState("");
    const [model, setModel] = useState("");

    const fetchModels = async () => {
        const url = "http://localhost:8100/api/models/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    };

    useEffect(() => {
        fetchModels();
    }, []);

    const handleColorChange = (event) => {
        setColor(event.target.value);
    };

    const handleYearChange = (event) => {
        setYear(event.target.value);
    };

    const handleVinChange = (event) => {
        setVin(event.target.value);
    };

    const handleModelChange = (event) => {
        setModel(event.target.value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = "http://localhost:8100/api/automobiles/";
        const data = {
            color: color,
            year: year,
            vin: vin,
            model_id: model,
        };
        const response = await fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
        });
        if (response.ok) {
            setColor("");
            setYear("");
            setVin("");
            setModel("");
        }
        const formTag = document.getElementById("create-automobile-form");
        formTag.reset();
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create an Automobile</h1>
                    <form onSubmit={handleSubmit} id="create-automobile-form">
                        <div className="row g-2 mb-4">
                            <div className="col-md">
                                <div className="form-floating">
                                    <input
                                        onChange={handleColorChange}
                                        type="text"
                                        className="form-control"
                                        id="color"
                                        value={color}
                                    />
                                    <label htmlFor="color">Color</label>
                                </div>
                            </div>
                            <div className="col-md">
                                <div className="form-floating">
                                    <input
                                        onChange={handleYearChange}
                                        type="text"
                                        className="form-control"
                                        id="year"
                                        value={year}
                                    />
                                    <label htmlFor="year">Year</label>
                                </div>
                            </div>
                        </div>
                        <div className="col-md mb-4">
                            <div className="form-floating">
                                <input
                                    onChange={handleVinChange}
                                    type="text"
                                    maxLength={17}
                                    className="form-control"
                                    id="vin"
                                    value={vin}
                                />
                                <label htmlFor="vin">Vin</label>
                            </div>
                        </div>
                        <div className="dropdown mb-3">
                            <select
                                value={model}
                                onChange={handleModelChange}
                                className="form-select form-select-lg mb-3"
                                aria-label="Large select example"
                            >
                                <option value="">Select Model</option>
                                {models.map((model) => {
                                    return (
                                        <option key={model.id} value={model.id}>
                                            {model.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CreateAutomobile;
