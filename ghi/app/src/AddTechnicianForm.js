import React, { useState } from "react";

function AddTechnicianForm() {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [employeeID, setEmployeeID] = useState("");

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    };

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    };

    const handleEmployeeIDChange = (event) => {
        const value = event.target.value;
        setEmployeeID(value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeID;

        const techniciansUrl = "http://localhost:8080/api/technicians/";

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: { "Content-Type": "application/json" },
        };

        const technicianResponse = await fetch(techniciansUrl, fetchConfig);

        if (technicianResponse.ok) {
            const newTechnician = await technicianResponse.json();
            setFirstName("");
            setLastName("");
            setEmployeeID("");
        }

        const formElement = document.getElementById("create-technician-form");
        formElement.reset();
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a new Technician</h1>
                    <form id="create-technician-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input
                                value={firstName}
                                onChange={handleFirstNameChange}
                                placeholder="First Name"
                                name="first-name"
                                required
                                type="text"
                                id="first-name"
                                className="form-control"
                            />
                            <label htmlFor="first-name">First Name</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input
                                value={lastName}
                                onChange={handleLastNameChange}
                                placeholder="Last Name"
                                name="last-name"
                                required
                                type="text"
                                id="last-name"
                                className="form-control"
                            />
                            <label htmlFor="last-name">Last Name</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input
                                value={employeeID}
                                onChange={handleEmployeeIDChange}
                                placeholder="Employee ID"
                                name="employeeID"
                                required
                                type="text"
                                id="employeeID"
                                className="form-control"
                            />
                            <label htmlFor="employeeID">Employee ID</label>
                        </div>

                        <button className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default AddTechnicianForm;
