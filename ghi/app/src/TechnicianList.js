import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";

function TechnicianList() {
    const [technicians, setTechnicians] = useState([]);

    const fetchTechnicians = async () => {
        const url = "http://localhost:8080/api/technicians/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    };

    useEffect(() => {
        fetchTechnicians();
    }, []);

    return (
        <>
            {technicians.length ? (
                <div>
                    <br></br>
                    <p className="h1">Technician List</p>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Employee ID</th>
                            </tr>
                        </thead>
                        <tbody>
                            {technicians.map((technician) => {
                                return (
                                    <tr key={technician.id}>
                                        <td>{technician.first_name}</td>
                                        <td>{technician.last_name}</td>
                                        <td>{technician.employee_id}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            ) : (
                <>
                    <h1>No Technicians</h1>
                    <p>There are no technicians in the database</p>
                    <NavLink to="/technicians/add" className="btn btn-success">
                        Add a Technician
                    </NavLink>
                </>
            )}
        </>
    );
}

export default TechnicianList;
