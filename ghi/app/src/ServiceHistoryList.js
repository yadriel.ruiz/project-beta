import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";

function ServiceHistoryList() {
    const [searchedAppointments, setSearchedAppointments] = useState([]);
    const [appointments, setAppointments] = useState([]);

    const formatDateTime = (dateTimeString) => {
        const dateTime = new Date(dateTimeString);
        return dateTime.toLocaleString();
    };

    const handleSearch = () => {
        const searchedVIN = document.getElementById("vin-number").value;
        const searchedAppointments = appointments.filter(
            (appointment) => appointment.vin === searchedVIN
        );
        setSearchedAppointments(searchedAppointments);
    };

    const fetchAppointments = async () => {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        }
    };

    useEffect(() => {
        fetchAppointments();
    }, []);

    return (
        <>
            {appointments.length ? (
                <div>
                    <br></br>
                    <p className="h1">Service History</p>
                    <input
                        id="vin-number"
                        type="text"
                        placeholder="Search by VIN..."
                    ></input>
                    <button onClick={handleSearch}>Search</button>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Customer</th>
                                <th>VIN</th>
                                <th>Date and Time</th>
                                <th>Technician</th>
                                <th>Reason</th>
                                <th>Ending Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {searchedAppointments.length > 0
                                ? searchedAppointments.map((appointment) => (
                                        <tr key={appointment.id}>
                                            <td>{appointment.customer}</td>
                                            <td>{appointment.vin}</td>
                                            <td>
                                                {formatDateTime(
                                                    appointment.date_time
                                                )}
                                            </td>
                                            <td>{appointment.technician}</td>
                                            <td>{appointment.reason}</td>
                                            <td>{appointment.status}</td>
                                        </tr>
                                    ))
                                    : appointments.map((appointment) => (
                                        <tr key={appointment.id}>
                                            <td>{appointment.customer}</td>
                                            <td>{appointment.vin}</td>
                                            <td>
                                                {formatDateTime(
                                                    appointment.date_time
                                                )}
                                            </td>
                                            <td>{appointment.technician}</td>
                                            <td>{appointment.reason}</td>
                                            <td>{appointment.status}</td>
                                        </tr>
                                    ))}
                        </tbody>
                    </table>
                </div>
            ) : (
                <>
                    <h1>No Appointments</h1>
                    <p>
                        There are no service history appointments in the
                        database
                    </p>
                    <NavLink
                        to="/appointments/create"
                        className="btn btn-success"
                    >
                        Add a Service Appointment
                    </NavLink>
                </>
            )}
        </>
    );
}

export default ServiceHistoryList;
