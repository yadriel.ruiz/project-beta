import React, { useState } from "react";

function CreateManufacturer() {
    const [name, setName] = useState("");

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;

        const manufacturerURL = "http://localhost:8100/api/manufacturers/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: { "Content-Type": "application/json" },
        };

        try {
            const manufacturerResponse = await fetch(
                manufacturerURL,
                fetchConfig
            );

            if (manufacturerResponse.ok) {
                const newManufacturer = await manufacturerResponse.json();
                setName("");
                window.alert("Manufacturer created successfully.");
            } else {
                throw new Error("Failed to add manufacturer");
            }
        } catch (error) {
            console.error("Error occurred while submitting data:", error);
            window.alert("This manufacturer is already saved in our database.");
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Manufacturer</h1>
                    <form id="create-technician-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input
                                value={name}
                                onChange={handleNameChange}
                                placeholder="Manufacturer Name"
                                name="name"
                                required
                                type="text"
                                id="datetime"
                                className="form-control"
                            />
                            <label htmlFor="datetime">Manufacturer Name</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CreateManufacturer;
