import React, { useState, useEffect } from "react";

function CreateAppointmentForm() {
    const [technicians, setTechnicians] = useState([]);
    const [dateTime, setDateTime] = useState("");
    const [reason, setReason] = useState("");
    const [status, setStatus] = useState("");
    const [vin, setVin] = useState("");
    const [customer, setCustomer] = useState("");
    const [technician, setTechnician] = useState("");

    const handleDateTimeChange = (event) => {
        const value = event.target.value;
        setDateTime(value);
    };

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    };

    const handleStatusChange = (event) => {
        const value = event.target.value;
        setStatus(value);
    };

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    };

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    };

    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    };

    const fetchData = async () => {
        const url = "http://localhost:8080/api/technicians/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.date_time = dateTime;
        data.reason = reason;
        data.status = status;
        data.vin = vin;
        data.customer = customer;
        data.technician = technician;

        const appointmentURL = "http://localhost:8080/api/appointments/";

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: { "Content-Type": "application/json" },
        };

        const appointmentResponse = await fetch(appointmentURL, fetchConfig);

        if (appointmentResponse.ok) {
            const newAppointment = await appointmentResponse.json();
            setDateTime("");
            setReason("");
            setStatus("");
            setVin("");
            setCustomer("");
            setTechnician("");
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Appointment</h1>
                    <form id="create-technician-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input
                                value={dateTime}
                                onChange={handleDateTimeChange}
                                placeholder="Date and Time"
                                name="name"
                                required
                                type="datetime-local"
                                id="datetime"
                                className="form-control"
                            />
                            <label htmlFor="datetime">Date and Time</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input
                                value={reason}
                                onChange={handleReasonChange}
                                placeholder="Reason"
                                name="name"
                                required
                                type="text"
                                id="first-name"
                                className="form-control"
                            />
                            <label htmlFor="first-name">Reason</label>
                        </div>

                        <div className="form-floating mb-3">
                            <select
                                value={status}
                                onChange={handleStatusChange}
                                required
                                id="status"
                                className="form-select"
                                name="status"
                            >
                                <option value={""}>Choose a Status</option>
                                <option value={"created"}>Created</option>
                            </select>
                        </div>

                        <div className="form-floating mb-3">
                            <input
                                value={vin}
                                onChange={handleVinChange}
                                placeholder="VIN Number"
                                name="name"
                                required
                                type="text"
                                id="vin-number"
                                className="form-control"
                            />
                            <label htmlFor="vin-number">VIN Number</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input
                                value={customer}
                                onChange={handleCustomerChange}
                                placeholder="Customer"
                                name="name"
                                required
                                type="text"
                                id="customer"
                                className="form-control"
                            />
                            <label htmlFor="customer">Customer</label>
                        </div>

                        <div className="form-floating mb-3">
                            <select
                                value={technician}
                                onChange={handleTechnicianChange}
                                required
                                id="technician"
                                className="form-select"
                                name="location"
                            >
                                <option value={""}>Choose a Technician</option>
                                {technicians.map((technician) => {
                                    return (
                                        <option
                                            key={technician.employee_id}
                                            value={technician.id}
                                        >
                                            {technician.first_name}{" "}
                                            {technician.last_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CreateAppointmentForm;
