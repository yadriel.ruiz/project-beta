import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";

function SalesPeopleList() {
    const [salesperson, setSalesperson] = useState([]);
    const fetchSalesperson = async () => {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalesperson(data.salesperson);
        }
    };
    useEffect(() => {
        fetchSalesperson();
    }, []);

    return (
        <>
            {salesperson.length ? (
                <div className="mt-3">
                    <h1>Salespeople</h1>
                    <table className="table table-striped mt-3">
                        <thead>
                            <tr>
                                <th>Employee ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            {salesperson.map((employee) => {
                                return (
                                    <tr key={employee.id}>
                                        <td>{employee.employee_id}</td>
                                        <td>{employee.first_name}</td>
                                        <td>{employee.last_name}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            ) : (
                <>
                    <h1>No Salespeople</h1>
                    <p>There are no salespeople in the database</p>
                    <NavLink to="/salespeople/add" className="btn btn-success">
                        Add a Salesperson
                    </NavLink>
                </>
            )}
        </>
    );
}
export default SalesPeopleList;
