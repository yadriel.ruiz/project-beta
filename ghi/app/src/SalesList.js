import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";

function SalesList() {
    const [sales, setSales] = useState([]);

    const getSales = async () => {
        const url = "http://localhost:8090/api/sales/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    };

    useEffect(() => {
        getSales();
    }, []);

    return (
        <>
            {sales.length ? (
                <div className="mt-3">
                    <h1>Salespeople</h1>
                    <table className="table table-striped mt-3">
                        <thead>
                            <tr>
                                <th>Salesperson Employee ID</th>
                                <th>Salesperson Name</th>
                                <th>Customer</th>
                                <th>Vin</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            {sales.map((sale) => {
                                return (
                                    <tr key={sale.id}>
                                        <td>{sale.employee_id}</td>
                                        <td>{sale.salesperson}</td>
                                        <td>{sale.customer}</td>
                                        <td>{sale.automobile}</td>
                                        <td>{sale.price}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            ) : (
                <>
                    <h1>No Sales</h1>
                    <p>There are no sales in the database</p>
                    <NavLink to="/sales/add" className="btn btn-success">
                        Add a Sale
                    </NavLink>
                </>
            )}
        </>
    );
}

export default SalesList;
