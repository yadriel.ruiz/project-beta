import React, { useState } from "react";

function CustomerForm() {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [address, setAddress] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");

    const handleFirstNameChange = (event) => {
        setFirstName(event.target.value);
    };

    const handleLastNameChange = (event) => {
        setLastName(event.target.value);
    };

    const handleAddressChange = (event) => {
        setAddress(event.target.value);
    };

    const handlePhoneNumberChange = (event) => {
        setPhoneNumber(event.target.value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.address = address;
        data.phone_number = phoneNumber;
        const url = "http://localhost:8090/api/customers/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const customerResponse = await fetch(url, fetchConfig);
        if (customerResponse.ok) {
            const customerData = await customerResponse.json();
            setFirstName("");
            setLastName("");
            setAddress("");
            setPhoneNumber("");
        }
        const formTag = document.getElementById("create-customer-form");
        formTag.reset();
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Customer</h1>
                    <form onSubmit={handleSubmit} id="create-customer-form">
                        <div className="row g-2 mb-4">
                            <div className="col-md">
                                <div className="form-floating">
                                    <input
                                        onChange={handleFirstNameChange}
                                        type="text"
                                        className="form-control"
                                        id="firstName"
                                        value={firstName}
                                    />
                                    <label htmlFor="firstName">
                                        First Name
                                    </label>
                                </div>
                            </div>
                            <div className="col-md">
                                <div className="form-floating">
                                    <input
                                        onChange={handleLastNameChange}
                                        type="text"
                                        className="form-control"
                                        id="lastName"
                                        value={lastName}
                                    />
                                    <label htmlFor="lastName">Last Name</label>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 mb-4">
                            <div className="col-md mb-4">
                                <div className="form-floating">
                                    <input
                                        onChange={handleAddressChange}
                                        type="text"
                                        className="form-control"
                                        id="address"
                                        value={address}
                                    />
                                    <label htmlFor="address">Address</label>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 mb-4">
                            <div className="col-md mb-4">
                                <div className="form-floating">
                                    <input
                                        onChange={handlePhoneNumberChange}
                                        type="text"
                                        className="form-control"
                                        id="phoneNumber"
                                        value={phoneNumber}
                                    />
                                    <label htmlFor="phoneNumber">
                                        Phone Number
                                    </label>
                                </div>
                            </div>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CustomerForm;
