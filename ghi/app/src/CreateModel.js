import React, { useState, useEffect } from "react";

function CreateModel() {
    const [manufacturers, setManufacturers] = useState([]);
    const [Name, setName] = useState("");
    const [picture, setPicture] = useState("");
    const [manufacturer, setManufacturer] = useState("");

    const fetchManufacturers = async () => {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    };

    useEffect(() => {
        fetchManufacturers();
    }, []);

    const handleNameChange = (event) => {
        setName(event.target.value);
    };

    const handlePictureChange = (event) => {
        setPicture(event.target.value);
    };

    const handleManufacturerChange = (event) => {
        setManufacturer(event.target.value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = "http://localhost:8100/api/models/";
        const data = {
            name: Name,
            picture_url: picture,
            manufacturer_id: manufacturer,
        };
        const response = await fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
        });
        if (response.ok) {
            setName("");
            setPicture("");
            setManufacturer("");
        }
        const formTag = document.getElementById("create-model-form");
        formTag.reset();
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Model</h1>
                    <form onSubmit={handleSubmit} id="create-model-form">
                        <div className="row g-2 mb-4">
                            <div className="col-md">
                                <div className="form-floating">
                                    <input
                                        onChange={handleNameChange}
                                        type="text"
                                        className="form-control"
                                        id="Name"
                                        value={Name}
                                    />
                                    <label htmlFor="Name">Name</label>
                                </div>
                            </div>
                            <div className="col-md">
                                <div className="form-floating">
                                    <input
                                        onChange={handlePictureChange}
                                        type="url"
                                        className="form-control"
                                        id="picture"
                                        value={picture}
                                    />
                                    <label htmlFor="picture">Picture Url</label>
                                </div>
                            </div>
                        </div>
                        <div className="dropdown mb-3">
                            <select
                                value={manufacturer}
                                onChange={handleManufacturerChange}
                                className="form-select form-select-lg mb-3"
                                aria-label="Large select example"
                            >
                                <option value="">Select Manufacturer</option>
                                {manufacturers.map((make) => {
                                    return (
                                        <option key={make.id} value={make.id}>
                                            {make.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CreateModel;
