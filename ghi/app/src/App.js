import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AddTechnicianForm from './AddTechnicianForm';
import TechnicianList from './TechnicianList';
import CreateAppointmentForm from './CreateAppointmentForm';
import AppointmentList from './AppointmentList';
import ServiceHistoryList from './ServiceHistoryList';
import CustomerForm from './CustomerForm';
import SalespersonForm from './SalespersonForm';
import ManufacturerList from './ManufacturerList';
import ModelList from './ModelList';
import AutomobilesList from './AutomobilesList';
import CreateManufacturer from './CreateManufacturerForm';
import NewSaleForm from './NewSaleForm';
import SalesPeopleList from './SalespeopleList';
import CustomerList from './CustomerList';
import SalesList from './SalesList';
import SalesHistory from './SalesHistory'
import CreateModel from './CreateModel';
import CreateAutomobile from './CreateAutomobile';


function App() {

return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} /> 
          <Route path="technicians">
            <Route index={true} element={<TechnicianList />} />
            <Route path="add" element={<AddTechnicianForm />} />
          </Route>
          <Route path="appointments">
            <Route index={true} element={<AppointmentList />} />
            <Route path="create" element={<CreateAppointmentForm />} />
            <Route path="history" element={<ServiceHistoryList />} />
          </Route>
          <Route path="manufacturers" >
            <Route index={true} element={<ManufacturerList />} />
            <Route path="create" element={<CreateManufacturer />} />
          </Route>
          <Route path="models">
            <Route index={true} element={<ModelList />} />
            <Route path="create" element={<CreateModel />} />
          </Route>
          <Route path="automobiles">
            <Route index={true} element={<AutomobilesList />} />
            <Route path="create" element={<CreateAutomobile />} />
          </Route>
          <Route path="salespeople" >
            <Route index={true} element={<SalesPeopleList />} />
            <Route path="add" element={<SalespersonForm />} />
            <Route path="history" element={<SalesHistory />} />
          </Route>
            <Route path="customers" >
            <Route index={true} element={<CustomerList />} />
            <Route path="add" element={<CustomerForm />} />
          </Route>
          <Route path="sales" >
            <Route index={true} element={<SalesList />} />
            <Route path="/sales/add" element={<NewSaleForm/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
