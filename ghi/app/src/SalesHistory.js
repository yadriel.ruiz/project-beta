import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";

function SalesHistory() {
    const [salespeople, setSalespeople] = useState([]);
    const [sales, setSales] = useState([]);
    const [originalSales, setOriginalSales] = useState([]);

    const fetchSalespeople = async () => {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salesperson);
        }
    };

    const fetchSales = async () => {
        const url = "http://localhost:8090/api/sales/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
            setOriginalSales(data.sales);
        }
    };

    useEffect(() => {
        fetchSalespeople();
        fetchSales();
    }, []);

    const handleSalespersonChange = (event) => {
        const salespersonId = event.target.value;
        const filteredSales = originalSales.filter(
            (sale) => sale.salesperson === salespersonId
        );
        setSales(filteredSales);
    };

    return (
        <>
            {sales.length ? (
                <div className="mt-3">
                    <h1>Salespeople</h1>
                    <div className="dropdown mb-3">
                        <select
                            onChange={handleSalespersonChange}
                            className="form-select form-select-lg mb-3"
                            aria-label="Large select example"
                        >
                            <option value="">
                                Select Sales Representative
                            </option>
                            {salespeople.map((salesperson) => {
                                return (
                                    <option
                                        key={salesperson.id}
                                        value={
                                            salesperson.first_name +
                                            " " +
                                            salesperson.last_name
                                        }
                                    >
                                        {salesperson.first_name}{" "}
                                        {salesperson.last_name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <table className="table table-striped mt-3">
                        <thead>
                            <tr>
                                <th>Salesperson</th>
                                <th>Customer</th>
                                <th>Vin</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            {sales.map((sale) => {
                                return (
                                    <tr key={sale.id}>
                                        <td>{sale.salesperson}</td>
                                        <td>{sale.customer}</td>
                                        <td>{sale.automobile}</td>
                                        <td>{sale.price}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            ) : (
                <>
                    <h1>No Sales</h1>
                    <p>There are no sales in the database</p>
                    <NavLink to="/sales/add" className="btn btn-success">
                        Add a Sale
                    </NavLink>
                </>
            )}
        </>
    );
}

export default SalesHistory;
