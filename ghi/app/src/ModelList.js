import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";

function ModelList() {
    const [models, setModels] = useState([]);
    const fetchModels = async () => {
        const url = "http://localhost:8100/api/models/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    };

    useEffect(() => {
        fetchModels();
    }, []);

    return (
        <>
            {models.length ? (
                <div>
                    <br></br>
                    <p className="h1">Model List</p>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Manufacturer</th>
                                <th>Picture</th>
                            </tr>
                        </thead>
                        <tbody>
                            {models.map((model) => {
                                return (
                                    <tr key={model.id}>
                                        <td>{model.name}</td>
                                        <td>{model.manufacturer.name}</td>
                                        <td>
                                            <img
                                                height={200}
                                                src={model.picture_url}
                                            ></img>
                                        </td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            ) : (
                <>
                    <h1>No Car Models</h1>
                    <p>There are no car models in the database</p>
                    <NavLink to="/models/create" className="btn btn-success">
                        Add a Model
                    </NavLink>
                </>
            )}
        </>
    );
}

export default ModelList;
