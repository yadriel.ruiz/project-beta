import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";

function CustomerList() {
    const [customers, setCustomers] = useState([]);

    const fetchCustomers = async () => {
        const url = "http://localhost:8090/api/customers/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    };

    useEffect(() => {
        fetchCustomers();
    }, []);

    return (
        <>
            {customers.length ? (
                <div className="mt-3">
                    <h1>Customers</h1>
                    <table className="table table-striped mt-3">
                        <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Phone Number</th>
                                <th>Address</th>
                            </tr>
                        </thead>
                        <tbody>
                            {customers.map((customer) => {
                                return (
                                    <tr key={customer.id}>
                                        <td>{customer.first_name}</td>
                                        <td>{customer.last_name}</td>
                                        <td>{customer.phone_number}</td>
                                        <td>{customer.address}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            ) : (
                <>
                    <h1>No Customers</h1>
                    <p>There are no customers in the database</p>
                    <NavLink to="/customers/add" className="btn btn-success">
                        Add a Customer
                    </NavLink>
                </>
            )}
        </>
    );
}

export default CustomerList;
