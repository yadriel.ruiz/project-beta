from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Technician, Appointment
from .encoders import TechnicianEncoder, AppointmentListEncoder
import json


@require_http_methods(["POST", "GET"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET"])
def api_technician_detail(request, id):
    if request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=id)
            count, _ = technician.delete()
        except Technician.DoesNotExist:
            return JsonResponse(
                {"error": "Selected technician does not exist."}
                )
        return JsonResponse({"deleted": count > 0})
    else:
        try:
            technician = Technician.objects.get(id=id)
        except Technician.DoesNotExist:
            return JsonResponse(
                {"error": "Selected technician does not exist."}
                )
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )


@require_http_methods(["POST", "GET"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.get(id=content["technician"])
        content["technician"] = technician
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET"])
def api_appointment_detail(request, id):
    if request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=id)
            count, _ = appointment.delete()
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"error": "Selected appointment does not exist."},
                status=400
                )
        return JsonResponse({"deleted": count > 0})
    else:
        try:
            appointment = Appointment.objects.get(id=id)
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"error": "Selected appointment does not exist."},
                status=400
                )
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False
        )


@require_http_methods("PUT")
def api_cancel_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.status = "cancelled"
        appointment.save()
    except Appointment.DoesNotExist:
        return JsonResponse({"error": "Selected appointment does not exist."})
    return JsonResponse(
        appointment,
        encoder=AppointmentListEncoder,
        safe=False
    )


@require_http_methods("PUT")
def api_finish_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.status = "finished"
        appointment.save()
    except Appointment.DoesNotExist:
        return JsonResponse({"error": "Selected appointment does not exist."})
    return JsonResponse(
        appointment,
        encoder=AppointmentListEncoder,
        safe=False
    )
