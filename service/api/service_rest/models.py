from django.db import models


class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)
    sold = models.BooleanField()


class Appointment(models.Model):
    STATUSES = (
        ("created", "CREATED"),
        ("cancelled", "CANCELLED"),
        ("finished", "FINISHED")
    )
    date_time = models.DateTimeField()
    reason = models.TextField()
    status = models.CharField(
        max_length=50,
        choices=STATUSES,
        default="created"
        )
    vin = models.CharField(max_length=50)
    customer = models.CharField(max_length=50)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
        null=True
    )
