# Generated by Django 4.0.3 on 2024-03-20 14:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0003_alter_sale_price'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sale',
            name='price',
            field=models.CharField(max_length=20),
        ),
    ]
