from .models import Salesperson, Sale, Customer
from common.json import ModelEncoder


class SalesPersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "automobile",
        "salesperson",
        "customer",
        "price",
    ]

    def get_extra_data(self, o):
        return {
            "automobile": o.automobile.vin,
            "salesperson": o.salesperson.first_name + " " + o.salesperson.last_name,
            "customer": o.customer.first_name + " " + o.customer.last_name,
            "employee_id": o.salesperson.employee_id,
        }
