from django.urls import path
from .views import (
    list_salespersons,
    list_customers,
    list_sales,
    delete_sale,
    delete_customer,
    delete_salesperson,
)

urlpatterns = [
    path("salespeople/", list_salespersons, name="list_salespersons"),
    path("customers/", list_customers, name="list_customers"),
    path("sales/", list_sales, name="list_sales"),
    path("sales/<int:id>/", delete_sale, name="delete_sale"),
    path("customers/<int:id>/", delete_customer, name="delete_customer"),
    path("salespeople/<int:id>/", delete_salesperson, name="delete_salesperson"),
]
