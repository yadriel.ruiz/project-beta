from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import AutomobileVO, Salesperson, Sale, Customer
import json
from django.http import JsonResponse
from .encoder import SalesPersonEncoder, CustomerEncoder, SaleEncoder


@require_http_methods(["GET", "POST"])
def list_salespersons(request):
    if request.method == "GET":
        salesperson = Salesperson.objects.all()
        return JsonResponse(
            {"salesperson": salesperson},
            encoder=SalesPersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Could not create the salesperson"})
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Could not create the customer"})
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            salesperson = Salesperson.objects.get(id=content["salesperson"])
            customer = Customer.objects.get(id=content["customer"])
            content["automobile"] = automobile
            content["salesperson"] = salesperson
            content["customer"] = customer
        except:
            if AutomobileVO.DoesNotExist:
                response = JsonResponse({"message": "Automobile does not exist"})
                response.status_code = 400
                return response
            if Salesperson.DoesNotExist:
                response = JsonResponse({"message": "Salesperson does not exist"})
                response.status_code = 400
                return response
            if Customer.DoesNotExist:
                response = JsonResponse({"message": "Customer does not exist"})
                response.status_code = 400
                return response

        sales = Sale.objects.create(**content)
        return JsonResponse(
            sales,
            encoder=SaleEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def delete_salesperson(request, id):
    try:
        salesperson = Salesperson.objects.get(id=id)
    except Salesperson.DoesNotExist:
        return JsonResponse({"message": "Does not exist"})
    if request.method == "DELETE":
        salesperson.delete()
        return JsonResponse({"message": "Salesperson Deleted"})


@require_http_methods(["DELETE"])
def delete_customer(request, id):
    try:
        customer = Customer.objects.get(id=id)
    except Customer.DoesNotExist:
        return JsonResponse({"message": "Does not exist"})
    if request.method == "DELETE":
        customer.delete()
        return JsonResponse({"message": "Customer Deleted"})


@require_http_methods(["DELETE"])
def delete_sale(request, id):
    try:
        sale = Sale.objects.get(id=id)
    except Sale.DoesNotExist:
        return JsonResponse({"message": "Does not exist"})
    if request.method == "DELETE":
        sale.delete()
        return JsonResponse({"message": "Sale Deleted"})
