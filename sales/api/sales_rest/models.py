from django.db import models


class Salesperson(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    employee_id = models.CharField(max_length=10, unique=True)


class Customer(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    address = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=15)


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField()


class Sale(models.Model):
    salesperson = models.ForeignKey(
        Salesperson, related_name="salesperson", on_delete=models.CASCADE
    )
    customer = models.ForeignKey(
        Customer, related_name="customer", on_delete=models.CASCADE
    )
    automobile = models.ForeignKey(
        AutomobileVO, related_name="automobile", on_delete=models.CASCADE
    )
    price = models.CharField(max_length=20)
