from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path("api/", include("sales_rest.urls")),
    path("admin/", admin.site.urls),
]
